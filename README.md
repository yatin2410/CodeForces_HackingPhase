# CodeForces_HackingPhase
Script for hacking people's codes After Educational Round(Hacking Phase) on Codeforces
It'll Check For specified Input and Output for all problems.
It'll Only Run C++, Java and Python Codes. (But You can always add other language in code)

How To Use :-
1) Download Zip File.
2) Make all input files like - "contestcode+problemcode" (eg:- 953A )
3) Make all Output files like - "contestcode+problemcode+ans" (eg:- 953Aans )
4) Run 'Get_Run_Codes.py' through terminal like this:
  $ python Get_Run_Codes.py
5) Now enter contest code and number of pages you want to run.
6) It'll Start checking codes from first page.
7) You'll find result in HackData file also outputs of possible hacks in CodesofHacks folder.

In case of multiple output possible, change checking code files... 

ENJOY!!!
